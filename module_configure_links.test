<?php

/**
 * @file
 * SimpleTest for Module configure links
 */

class ModuleConfigureLinks extends DrupalWebTestCase {
  protected $admin_user;

  public static function getInfo() {
    return array(
      'name' => 'Module configure links',
      'description' => 'Check Module configure links functionality.',
      'group' => 'Module configure links',
    );
  }

  public function setUp() {
    // Enable module_configure_links, plus two core modules.
    parent::setUp('module_configure_links', 'aggregator', 'comment');

    // Create admin user, with core module permissions.
    $admin_perms = array(
      'access administration pages',
      'administer modules',
      'administer news feeds',
      'administer comments',
    );
    $this->admin_user = $this->drupalCreateUser($admin_perms);
  }

  public function testContentTypeSettings() {
    $edit = array();

    // Login as admin
    $this->drupalLogin($this->admin_user);

    // Disable the core modules.
    $edit['modules[Core][aggregator][enable]'] = FALSE;
    $edit['modules[Core][comment][enable]'] = FALSE;
    $this->drupalPost('admin/modules', $edit, t('Save configuration'));

    //Enable single module
    $edit['modules[Core][aggregator][enable]'] = TRUE;
    $this->drupalPost('admin/modules', $edit, t('Save configuration'));

    //Check for message and redirect URL.
    $this->assertText(t('Redirected to Aggregator module configuration page.'), 'Found module enabled message.');
    $this->assertUrl('admin/config/services/aggregator/settings', array(), 'Found module enabled redirect.');

    // Disable the core module again.
    $edit['modules[Core][aggregator][enable]'] = FALSE;
    $this->drupalPost('admin/modules', $edit, t('Save configuration'));

    // Enable both modules.
    $edit['modules[Core][aggregator][enable]'] = TRUE;
    $edit['modules[Core][comment][enable]'] = TRUE;
    $this->drupalPost('admin/modules', $edit, t('Save configuration'));

    //Check if both links exist
    $this->assertLink(t('Aggregator'), 0, 'Found Aggregator configuration link');
    $this->assertLink(t('Comment'), 0, 'Found Comment configuration link');
  }
}
