Module Configure Links
by eosrei
8/15/2012
http://eosrei.net

You've just installed a module, but now you need to configure it. Where is that
new menu item?

You can check the README.txt or look at admin/by-module in Drupal 6. Drupal 7
made this slightly better by allowing modules to provide a configure link in
their info file. These configure links are shown on the module list, but it
becomes a hassle to locate that link once many modules are installed.

This module corrects this problem by redirecting to a module's configuration
page when a single module is enabled or displaying a list of configuration
pages when more than one is available.

FEATURES:
Integrated with Drush
D6 & D7 - Guesses configuration using the module's shortest admin path.
D7 - Checks configure setting in info file, default for D7.

FUTURE FEATURES:
Make redirect optional.
